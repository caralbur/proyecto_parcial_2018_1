#Escriba su makefiile en este archivo
#
# Integrante 1: Burgos Perez, Carolina
# Integrante 2: Moya Larrea, Luis

prueba: prueba.o borrar.o claves.o contieneClave.o crearHashTable.o get.o numeroElementos.o put.o remover.o valores.o hash.o
	gcc obj/prueba.o obj/borrar.o obj/claves.o obj/contieneClave.o obj/crearHashTable.o obj/get.o obj/numeroElementos.o obj/put.o obj/remover.o obj/valores.o obj/hash.o -o bin/prueba -g

prueba.o: src/prueba.c
	gcc -Wall -c -I include/ src/prueba.c -o obj/prueba.o

borrar.o: src/borrar.c
	gcc -Wall -c -I include/ src/borrar.c -o obj/borrar.o

claves.o: src/claves.c
	gcc -Wall -c -I include/ src/claves.c -o obj/claves.o

contieneClave.o: src/contieneClave.c
	gcc -Wall -c -I include/ src/contieneClave.c -o obj/contieneClave.o

crearHashTable.o: src/crearHashTable.c
	gcc -Wall -c -I include/ src/crearHashTable.c -o obj/crearHashTable.o

get.o: src/get.c
	gcc -Wall -c -I include/ src/get.c -o obj/get.o

numeroElementos.o: src/numeroElementos.c
	gcc -Wall -c -I include/ src/numeroElementos.c -o obj/numeroElementos.o

put.o: src/put.c
	gcc -Wall -c -I include/ src/put.c -o obj/put.o

remover.o: src/remover.c
	gcc -Wall -c -I include/ src/remover.c -o obj/remover.o

valores.o: src/valores.c
	gcc -Wall -c -I include/ src/valores.c -o obj/valores.o

hash.o: src/hash.c
	gcc -Wall -c src/hash.c -o obj/hash.o


.PHONY: clean
clean:
	rm bin/* obj/*

