#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

hashtable *crearHashTable(int numeroBuckets){

	if(numeroBuckets == 0){
		return NULL;
	} 

	int i = 0;

	hashtable* hash = (hashtable*)malloc(sizeof(hashtable));
	hash->id = rand()%(500) + 0;
	hash->elementos = 0;
	hash->numeroBuckets = numeroBuckets;
	
	hash->buckets = (objeto **)malloc(numeroBuckets*sizeof(objeto *));	
        
	for(i = 0; i < numeroBuckets; i++){
                hash->buckets[i] = (objeto *)malloc(sizeof(objeto));		
        }

	return hash;
}
