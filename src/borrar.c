#include <stdio.h>
#include "hashtable.h"

void borrar(hashtable *tabla){ 

	if(tabla == NULL){
		return ;
	}

	int i = 0;

	while(i < tabla->numeroBuckets){
		objeto *obj = tabla->buckets[i];
		
		while(obj){
			obj->clave = NULL;
			obj->valor = NULL;
			free(obj->clave);
			free(obj->valor);
			obj = obj->siguiente;
			
		}

		i++;
	}

	free(tabla);

}
